#!/bin/bash

docker run --rm -p8000:8000 -p8001:8001 -p8002:8002 -v /home/nuno/Documents/Projects/Kalameet/exports/triton:/models nvcr.io/nvidia/tritonserver:22.02-py3 tritonserver --model-repository=/models