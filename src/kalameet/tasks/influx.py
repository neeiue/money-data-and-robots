import pandas as pd
import numpy as np
import datetime

from kalameet.utils.drivers.influx import InfluxDriver

def get_influx_driver(org: str, token: str) -> InfluxDriver:

    return InfluxDriver(org=org, token=token)

def get_last_entry_from_measurement(bucket: str, measurement: str, org: str, token: str):

    driver = get_influx_driver(org=org, token=token)

    query: str = f'from(bucket:"{bucket}") \
        |> range(start: 0, stop: now()) \
        |> filter(fn:(r) => r._measurement == "{measurement}") \
        |> keep(columns: ["_time"]) \
        |> sort(columns: ["_time"], desc: false) \
        |> last(column: "_time")'

    results = driver.query_influx(query=query)

    try:

        last_record_datetime: datetime = results[0].records[0]['_time'] + datetime.timedelta(days=1)
        last_record_date = last_record_datetime.strftime("%Y-%m-%d")

    except Exception as e:

        last_record_date = '2018-01-01'

    return last_record_date

def get_last_n_days(bucket: str, measurement: str, org: str, token: str, n: int, mean: float, std: float):

    driver = get_influx_driver(org=org, token=token)

    query: str = f'from(bucket:"{bucket}") \
        |> range(start: -{n}d) \
        |> filter(fn:(r) => r._measurement == "{measurement}") \
        |> filter(fn:(r) => r._field == "Close")\
        |> group(columns: ["_time"])'

    results = driver.query_influx(query=query)

    values = []
    for table in results:
        for record  in table.records:
            values.append(record['_value'])
    
    close_values = np.array(values, dtype=np.float32).reshape((n, 1))

    close_values = (close_values - mean) / std
    return close_values

def get_from_date(bucket: str, measurement: str, org: str, token: str, date: str):

    driver = get_influx_driver(org=org, token=token)

    query: str = f'from(bucket:"{bucket}") \
        |> range(start: {date}) \
        |> filter(fn:(r) => r._measurement == "{measurement}") \
        |> filter(fn:(r) => r._field == "Close")\
        |> group(columns: ["_time"])'

    results = driver.query_influx(query=query)

    values = []
    dates = []

    for table in results:
        for record  in table.records:
            values.append(record['_value'])
            dates.append(record['_time'].strftime('%Y-%m-%d'))
   
    return values, dates

def write_bot_predictions(dates, values, bucket: str, measurement: str, org: str, token: str):

    driver = get_influx_driver(org=org, token=token)

    df = pd.DataFrame({'Date': dates, 'Actions': values})
    df['Date'] = pd.to_datetime(df['Date'])
    df.set_index('Date', inplace=True)

    driver.insert_influx(bucket=bucket, record=df, measurement=measurement)

def write_predictions(data, bucket: str, measurement: str, org: str, token: str, n: int, mean: float, std: float):

    driver = get_influx_driver(org=org, token=token)

    scaled_data = np.array(data) * std + mean
    start_date = datetime.datetime.now()

    dates = [(start_date + datetime.timedelta(days=i)).strftime('%Y-%m-%d') for i in range(n)]

    df = pd.DataFrame({'Date': dates, 'Predictions': scaled_data})
    df['Date'] = pd.to_datetime(df['Date'])
    df.set_index('Date', inplace=True)

    driver.insert_influx(bucket=bucket, record=df, measurement=measurement)

def get_data_to_csv(bucket: str, measurement: str, org: str, token: str):

    driver = get_influx_driver(org=org, token=token)

    query: str = f'from(bucket:"{bucket}") \
        |> range(start: 0, stop: now()) \
        |> filter(fn:(r) => r._measurement == "{measurement}")'

    results = driver.query_influx_dataframe(query=query)
    return results

def write_record(load_path: str, bucket: str, measurement: str, org: str, token: str):

    driver = get_influx_driver(org=org, token=token)
    
    df = pd.read_csv(load_path)

    df['Date'] = pd.to_datetime(df['Date'])
    df.set_index('Date', inplace=True)

    int_cols = df.select_dtypes('int')
    df[int_cols.columns] = df[int_cols.columns].astype('float')

    driver.insert_influx(bucket=bucket, record=df, measurement=measurement)
