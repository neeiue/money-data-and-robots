import numpy as np

from kalameet.tasks.api.kalameet_bot import infer_kalameet_bot

def simulate_environment(lag_size, stock_data, initial_money):

    num_shares = 0
    close_money = initial_money
    discount = np.array([0.99], dtype=np.float32)
    actions = []

    state = get_next_state(
        timestep=1,
        lag_size=lag_size,
        stock_data=stock_data,
        num_shares=num_shares,
        close_money=close_money
    )

    action = infer_kalameet_bot(
        observations=state, 
        discount=discount, 
        reward=np.array([0], dtype=np.float32), 
        step_type=np.array([0], dtype=np.int32))

    actions.append(action[0])

    for timestep in range(1, len(stock_data)):

        close_money, num_shares, reward = apply_action(
                                            action=action[0], 
                                            timestep=timestep, 
                                            stock_data=stock_data, 
                                            close_money=close_money,
                                            num_shares=num_shares, 
                                            initial_money=initial_money)

        state = get_next_state(
            timestep=timestep, 
            lag_size=lag_size, 
            num_shares=num_shares, 
            stock_data=stock_data,
            close_money=close_money
        )

        action = infer_kalameet_bot(
            observations=state, 
            discount=discount, 
            reward=np.array([reward], dtype=np.float32), 
            step_type=np.array([1], dtype=np.int32))

        actions.append(action[0])

    return actions
    
def apply_action(action, timestep, stock_data, close_money, num_shares, initial_money):

    if action == 1:

        stock = stock_data[timestep]
        num_shares += 1

        close_money -= stock

    if action == 2 and num_shares > 0:

        num_shares -= 1
        current_stock = stock_data[timestep]

        close_money += current_stock

    reward = close_money - initial_money

    return close_money, num_shares, reward

def get_next_state(timestep, lag_size, stock_data, num_shares, close_money):

    start = timestep - lag_size + 1

    if start >= 0:
        data_slice = stock_data[start:timestep + 1]

    else:
        data_slice = stock_data[0:timestep + 1]
        data_slice = np.pad(data_slice, (-start, 0), 'constant', constant_values=stock_data[0])

    diff_state = [data_slice[i + 1] - data_slice[i] for i in range(lag_size - 1)]
    new_state = np.concatenate(
        [diff_state, np.array([close_money, num_shares], dtype=np.float32)], dtype=np.float32)
    return np.array(new_state).reshape(1, -1)