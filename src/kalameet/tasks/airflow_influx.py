import pandas as pd
import numpy as np
import datetime

from src.kalameet.utils.drivers.influx import InfluxDriver as SrcDriver

def get_influx_src_driver(org: str, token: str) -> SrcDriver:

    return SrcDriver(org=org, token=token)

def get_last_entry_from_measurement(bucket: str, measurement: str, org: str, token: str):

    driver = get_influx_src_driver(org=org, token=token)

    query: str = f'from(bucket:"{bucket}") \
        |> range(start: 0, stop: now()) \
        |> filter(fn:(r) => r._measurement == "{measurement}") \
        |> keep(columns: ["_time"]) \
        |> sort(columns: ["_time"], desc: false) \
        |> last(column: "_time")'

    results = driver.query_influx(query=query)

    try:

        last_record_datetime: datetime = results[0].records[0]['_time'] + datetime.timedelta(days=1)
        last_record_date = last_record_datetime.strftime("%Y-%m-%d")

    except Exception as e:

        last_record_date = '2018-01-01'

    return last_record_date

def write_record(load_path: str, bucket: str, measurement: str, org: str, token: str):

    driver = get_influx_src_driver(org=org, token=token)
    
    df = pd.read_csv(load_path)

    df['Date'] = pd.to_datetime(df['Date'])
    df.set_index('Date', inplace=True)

    int_cols = df.select_dtypes('int')
    df[int_cols.columns] = df[int_cols.columns].astype('float')

    driver.insert_influx(bucket=bucket, record=df, measurement=measurement)
