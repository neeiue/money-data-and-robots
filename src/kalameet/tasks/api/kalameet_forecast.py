import numpy as np

import tritonclient.http as httpclient

MODEL_NAME = 'kalameet_forecast'

def infer_kalameet_forecast(
               input,
               headers=None,
               request_compression_algorithm=None,
               response_compression_algorithm=None):

    triton_client = httpclient.InferenceServerClient(
                url='localhost:8000', verbose=False)

    inputs = []
    outputs = []

    inputs.append(httpclient.InferInput('input_1', [1, 7, 1], "FP32"))
    
    inputs[0].set_data_from_numpy(input.reshape(-1, 7, 1), binary_data=False)

    outputs.append(httpclient.InferRequestedOutput('output_1', binary_data=False))
                         
    results = triton_client.infer(
        MODEL_NAME,
        inputs,
        outputs=outputs,
        headers=headers,
        request_compression_algorithm=request_compression_algorithm,
        response_compression_algorithm=response_compression_algorithm)

    return results.get_output('output_1')['data']