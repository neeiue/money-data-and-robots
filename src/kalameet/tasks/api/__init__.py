import yfinance as yf
import argparse

from pandas_datareader import data as pdr
from pathlib import Path

RAW_PATH = Path('data/01_raw')

yf.pdr_override()

def fetch_data(stock: str):

    df = pdr.get_data_yahoo(stock, start="2018-01-01").reset_index()
    df.to_csv(RAW_PATH.joinpath(f"{stock}.csv"), index=False)