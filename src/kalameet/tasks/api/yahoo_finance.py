import yfinance as yf
import click

from pandas_datareader import data as pdr
from pathlib import Path

RAW_PATH = Path('/home/nuno/Documents/Projects/Kalameet/data/01_raw')

yf.pdr_override()

@click.command()
@click.option('--stock', help='Stock to fetch data from.')
@click.option('--start', help='Date to start fecthing from. Follows format yyyy-mm-dd.')
def fetch_data_cli(stock: str, start: str):

    return fetch_data(stock=stock, start=start)

def fetch_data(stock: str, start: str):

    df = pdr.get_data_yahoo(stock, start=start).reset_index()
    df.to_csv(RAW_PATH.joinpath(f"{stock}.csv"))

if __name__=='__main__':
    fetch_data_cli()