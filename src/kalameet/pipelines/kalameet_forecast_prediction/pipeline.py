from kedro.pipeline import Pipeline, node
from .nodes import get_last_n_days, infer_kalameet_forecast, write_predictions

def create_pipeline():

    return Pipeline(nodes=[
        node(
            func=get_last_n_days,
            inputs=[
                'params:data_extraction.bucket', 
                'params:kalameet_forecast.measurement',
                'params:influx.org',
                'params:influx.influx_token',
                'params:kalameet_forecast.n',
                'params:kalameet_forecast.rolling_mean',
                'params:kalameet_forecast.rolling_std'
            ],
            outputs='close_values',
            name='get_last_n_days'
        ),
        node(
            func=infer_kalameet_forecast,
            inputs=['close_values'],
            outputs='close_predictions',
            name='infer_predictions'
        ),
        node(
            func=write_predictions,
            inputs=[
                'close_predictions',
                'params:data_extraction.bucket', 
                'params:kalameet_forecast.measurement',
                'params:influx.org',
                'params:influx.influx_token',
                'params:kalameet_forecast.n',
                'params:kalameet_forecast.rolling_mean',
                'params:kalameet_forecast.rolling_std'],
            outputs=None,
            name='write_predictions'
        )
    ])