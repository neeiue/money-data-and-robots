from kedro.pipeline import Pipeline, node
from .nodes import get_from_date, simulate_environment, write_bot_predictions

def create_pipeline():

    return Pipeline(nodes=[
        node(
            func=get_from_date,
            inputs=[
                'params:data_extraction.bucket', 
                'params:kalameet_bot.measurement',
                'params:influx.org',
                'params:influx.influx_token',
                'params:kalameet_bot.date'
            ],
            outputs=['records_values', 'records_dates'],
            name='get_from_date'
        ),
        node(
            func=simulate_environment,
            inputs=[
                'params:kalameet_bot.lag_size',
                'records_values',
                'params:kalameet_bot.initial_money'
            ],
            outputs='actions',
            name='simulate_environment'
        ),
        node(
            func=write_bot_predictions,
            inputs=[
                'records_dates',
                'actions',
                'params:data_extraction.bucket', 
                'params:kalameet_bot.measurement',
                'params:influx.org',
                'params:influx.influx_token'
            ],
            outputs=None,
            name='write_bot_predictions'
        )
    ])