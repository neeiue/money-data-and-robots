import numpy as np

from tf_agents.environments import py_environment
from tf_agents.specs import array_spec
from tf_agents import trajectories as ts


class KalameetStockEnv(py_environment.PyEnvironment):

    def __init__(self, stock_data: np.array, lag_size: int, initial_money: float, discount: float = 0.9):

        self.lag_size: int = lag_size

        self.close_money: float = initial_money
        self.initial_money: float = initial_money

        self.discount: float = discount

        self._episode_ended: bool = False

        self.stock_data: np.array = stock_data
        self.timestep = 1
        self.num_shares = 0

        self._state = self._get_next_state()

        self._action_spec = array_spec.BoundedArraySpec(
            shape=(), dtype=np.int32, minimum=0, maximum=2, name='action')

        self._observation_spec = array_spec.ArraySpec(
            shape=(lag_size + 1,), dtype=np.float32)

    def _get_next_state(self):

        start = self.timestep - self.lag_size + 1

        if start >= 0:
            data_slice = self.stock_data[start:self.timestep + 1]

        else:
            data_slice = self.stock_data[0:self.timestep + 1]
            data_slice = np.pad(data_slice, (-start, 0), 'constant', constant_values=self.stock_data[0])

        diff_state = [data_slice[i + 1] - data_slice[i] for i in range(self.lag_size - 1)]
        new_state = np.concatenate(
            [diff_state, np.array([self.close_money, self.num_shares], dtype=np.float32)])
        return np.array(new_state)

    def action_spec(self):
        return self._action_spec

    def observation_spec(self):
        return self._observation_spec

    def _reset(self):

        self.timestep = 1
        self._state = self._get_next_state()
        self.num_shares = 0

        self.close_money = self.initial_money

        self._episode_ended = False

        return ts.restart(self._state)

    def _step(self, action):

        if self._episode_ended:

            return self.reset()

        if action == 1:

            if self.close_money < 0:

                self._episode_ended = True
                return ts.termination(self._state, -100)

            stock = self.stock_data[self.timestep]
            self.num_shares += 1

            self.close_money -= stock

        if action == 2:

            if self.num_shares <= 0:
                
                self._episode_ended = True
                return ts.termination(self._state, -100)

            self.num_shares -= 1
            current_stock = self.stock_data[self.timestep]

            self.close_money += current_stock

        reward = self.close_money - self.initial_money

        self.timestep += 1

        if self.timestep == len(self.stock_data):

            self._episode_ended = True
            return ts.termination(self._state, reward)

        self._state = self._get_next_state()

        return ts.transition(self._state, reward=reward, discount=self.discount)
