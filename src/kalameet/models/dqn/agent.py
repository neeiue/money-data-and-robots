import tensorflow as tf

from tf_agents.agents.dqn import dqn_agent
from tf_agents.environments import tf_py_environment
from tf_agents.utils import common
from tf_agents.networks import categorical_q_network
from tf_agents.networks.q_network import QNetwork
from tf_agents.agents.categorical_dqn import categorical_dqn_agent

class DQNAgent:

    def __init__(self, learning_rate: float, env: tf_py_environment.TFPyEnvironment, n_step_update: int) -> None:
        
        self.learning_rate: float = learning_rate

        self.train_step_counter = tf.Variable(0)
        self.optimizer = tf.keras.optimizers.Adam(learning_rate)

        categorical_q_net = categorical_q_network.CategoricalQNetwork(
            input_tensor_spec=env.observation_spec(),
            action_spec=env.action_spec(),
            fc_layer_params = (40,)
        )
      
        self.agent = categorical_dqn_agent.CategoricalDqnAgent(
            time_step_spec=env.time_step_spec(),
            action_spec=env.action_spec(),
            categorical_q_network=categorical_q_net,
            optimizer=self.optimizer,
            n_step_update=n_step_update,
            epsilon_greedy=0.1,
            gamma=0.99,
            td_errors_loss_fn=common.element_wise_squared_loss,
            train_step_counter=self.train_step_counter)

        self.agent.initialize()
