import pandas as pd
import tensorflow as tf
import mlflow
from datetime import datetime
from kalameet.models.lstm.lstm import MultiLSTM
from kalameet.models.lstm.window import WindowGenerator

from typing import List

class Trainer:

    def __init__(self, 
        train_df: pd.DataFrame,
        val_df: pd.DataFrame,
        test_df: pd.DataFrame,
        input_width: int, 
        label_width: int, 
        label_columns: List[str],
        model: MultiLSTM,
        shift: int = 1) -> None:
        
        self.input_width = input_width
        self.label_width = label_width
        self.shit = shift
        self.train_df = train_df
        self.val_df = val_df
        self.test_df = test_df
        self.label_columns = label_columns

        self.model = model

        self.window = WindowGenerator(
            train_df=train_df,
            val_df=val_df,
            test_df=test_df,
            label_columns=label_columns,
            input_width=input_width,
            label_width=label_width,
            shift=shift)

        self.loss_object = tf.keras.losses.MeanAbsoluteError()
        self.optimizer = tf.keras.optimizers.Adam()

        self.train_loss = tf.keras.metrics.Mean(name='train_loss')
        self.train_mae = tf.keras.metrics.MeanAbsoluteError(name='train_mae')

        self.test_loss = tf.keras.metrics.Mean(name='test_loss')
        self.test_mae = tf.keras.metrics.MeanAbsoluteError(name='test_mae')


    @tf.function
    def train_step(self, input, labels):

        with tf.GradientTape() as tape:

            predictions = self.model(input, training=True)
            loss = self.loss_object(labels, predictions)

        gradients = tape.gradient(loss, self.model.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, self.model.trainable_variables))

        self.train_loss(loss)
        self.train_mae(labels, predictions)


    @tf.function
    def test_step(self, input, labels):

        predictions = self.model(input, training=False)
        t_loss = self.loss_object(labels, predictions)

        self.test_loss(t_loss)
        self.test_mae(labels, predictions)

    def train(self, num_iterations: int):

        current_time = datetime.now().strftime("%Y%m%d-%H%M%S")

        with mlflow.start_run():

            for epoch in range(num_iterations):

                self.train_loss.reset_states()
                self.train_mae.reset_states()
                self.test_loss.reset_states()
                self.test_mae.reset_states()

                for inputs, labels in self.window.train:
                    self.train_step(inputs, labels)

                for test_inputs, test_labels in self.window.val:
                    self.test_step(test_inputs, test_labels)

                mlflow.log_metric(key='Training Loss', value=self.train_loss.result().numpy(), step=epoch)
                mlflow.log_metric(key='Test Loss', value=self.test_loss.result().numpy(), step=epoch)
                mlflow.log_metric(key='Mean Absolute Error', value=self.train_mae.result().numpy(), step=epoch)
                mlflow.log_metric(key='Test Mean Absolute Error', value=self.test_mae.result().numpy(), step=epoch)

            mlflow.keras.save_model(self.model, f'exports/models/kalameet_forecast/{current_time}')
            mlflow.keras.log_model(self.model, f'exports/models/kalameet_forecast/{current_time}')