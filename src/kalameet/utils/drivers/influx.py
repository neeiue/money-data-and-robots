import influxdb_client

from influxdb_client.client.write_api import SYNCHRONOUS
from typing import Iterable

class InfluxDriver:

    def __init__(self, 
        org: str, 
        token: str, 
        host: str = 'localhost',
        port: int = 8086) -> None:
        
        self._url = f"http://{host}:{port}"
        self.org = org

        self.client = influxdb_client.InfluxDBClient(
            url=self._url, 
            org=org, 
            debug=True,
            token=token)

    def query_influx(self, query: str):

        query_api = self.client.query_api()

        return query_api.query(query=query, org=self.org)

    def query_influx_dataframe(self, query: str):

        query_api = self.client.query_api()

        return query_api.query_data_frame(query=query, org=self.org)

    def insert_influx(self, bucket: str,  measurement: str, record):

        write_api = self.client.write_api(write_options=SYNCHRONOUS)

        write_api.write(
            bucket=bucket, 
            org=self.org, 
            record=record, 
            data_frame_measurement_name=measurement,
            data_frame_tag_columns=[])

