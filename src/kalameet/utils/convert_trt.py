import tensorflow as tf

conversion_params = tf.experimental.tensorrt.ConversionParams(
    precision_mode='FP16')

converter = tf.experimental.tensorrt.Converter(
    input_saved_model_dir='exports/models/mlflow/lstm/data/model',
    conversion_params=conversion_params)

converter.convert()

converter.save('exports/trt/')